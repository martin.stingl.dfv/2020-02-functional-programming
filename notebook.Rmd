---
title: "R Meetup"
date: 2020-02-11
output:
  rmdformats::readthedown:
    highlight: pygments
    css: media/style.css
    df_print: paged
---

```{r message=FALSE, warning=FALSE}
library(tidyverse)
```

* What is a functional programming language?  
    * Imperative vs. declarative
    * Procedural vs. functional (lazy evaluation)
    * First-class language objects? (functions are data)
    * Mathemtical functions and recursion
* Strengths of functional programming?
    * No sideeffects
    * Ease of code analysis; proofable
    * Readability?!
* Weaknesses?
    * Memory intensive
    * In pure form, harder to use; not useful for data analysis

* Is R a functional language?  Does it enforce a functional style of programming?    
No, its a multiparadigm language. And it just encourages functional style of programming. 

# Functional programming

> “To understand computations in R, two slogans are helpful:  
>    Everything that exists is an object.  
>    Everything that happens is a function call."  
>
> — John Chambers

## Functions

Functions are objects. They can be

* passed as a parameters to other functions
* copied and modified
* dynamically created by other functions

And they know their context (closure)


```{r}
outer <- function(max) {
  inner <- function(min=1) {
    seq(min, max)
  }
  inner
}
```


```{r}
seq5 <- outer(5)
seq5
ls(envir = environment(seq5))
get("max", envir = environment(seq5))
```

```{r}
outer_10 <- outer
formals(outer_10)$max <- 10
seq10 <- outer_10()
seq10()

```

```{r}
data_frame <- data.frame
formals(data_frame)$stringsAsFactors <- FALSE
data.frame(chr = LETTERS) %>% as.tbl() %>% head(3)
data_frame(chr = LETTERS) %>% as.tbl() %>% head(3)
```


* Functions do lazy evaluation of parameters

```{r error=TRUE}
max <- 1
seq_missing <- outer()
seq_missing()
```




## Expressions

* Expressions are trees

```{r}
lobstr::ast(2*(3+4))
`*`(2,`+`(3,4))
```

These trees can be modified.
The expressions are lazily evaluated and can depend on the context.

## Operators

Also infix operators are just functions and can be modified

```{r error=TRUE}
## Taken from: https://github.com/romainfrancois/evil.R/
# get a random function from base when using ::
`::` <- function(a,b) {
    get( sample( ls("package:base"), 1 ), "package:base" )
}

base::mean(runif(10))
rm(`::`)
```

```{r}
## Taken from: https://github.com/romainfrancois/evil.R/
# this is VERY evil!
`{` <- function(...) NULL

my_cool_function <- function(a) {sum(a)}
my_cool_function(runif(10))
my_cool_function
rm(`{`)
```

# Higher-order functions

## Functionals

A functional is a function that takes a function as an input and returns a vector as output. 

Example: Numeric integration

```{r}
cube <- function(x, a,b) {
  a*x**3-b*x**2
}

integrate(cube, -1, 1, a=1, b=2)
```

## Function factories

A function factory is a function that makes functions from vector inputs.

```{r}
power_function <- function(exp) {
  function(x) {
    x ^ exp
  }
}

square <- power_function(2)
cube <- power_function(3)

square(2)
cube(2)
```


## Function operators

A function operator is a function that takes one (or more) functions as input and returns a function as output.

```{r}
not.null <- purrr::negate(is.null)

deviation <- function(p, y) {
  (y-p)
}
sq_err <- function(f) {
  function(...) mean(f(...)**2)
}

y <- c(0,1,2,4)

fit <- nlm(sq_err(deviation), p=c(0), y=y)
fit
```


```{r}
ggplot(data.frame(x=y, y=y), aes(x,y)) +
  geom_point() +
  geom_hline(yintercept = fit$estimate)
```


# purrr

## map

Example: apply linear regression to multiple datasets.

Using lists:

```{r}
mtcars %>%
  split(.$cyl) %>%
  map(~ lm(mpg ~ wt, data = .)) %>%
  map(summary) %>%
  map_dbl("r.squared")

```

Using data frames:

```{r}

mt_models <- mtcars %>%
  group_by(cyl) %>%
  nest() %>%
  mutate(model = map(data, ~ lm(mpg ~ wt, data = .)),
         summary = map(model, summary),
         r.sq = map_dbl(summary, "r.squared"))

mt_models

# And return to the full data frame
mt_models %>% 
  select(-model, -summary) %>% 
  unnest()

```

## walk

```{r}

dir.create(temp <- tempfile())
iris %>%
  split(.$Species) %>%
  iwalk(~ write.csv(.x, file = file.path(temp, paste0(.y, ".csv"))))
list.files(temp, pattern = "csv$")
unlink(temp, recursive = TRUE)


```


```{r}

dir.create(temp <- tempfile())
iris %>%
  group_by(Species) %>%
  group_walk(~ write.csv(.x, file = file.path(temp, paste0(.y$Species, ".csv"))))
list.files(temp, pattern = "csv$")
unlink(temp, recursive = TRUE)


```

## pmap

```{r}

iris %>% 
  mutate(sum = pmap_dbl(select(., -Species), sum))

```


```{r}
calculations <- tibble::tribble(
  ~spec,        ~var,           ~agg,
  "setosa",     "Petal.Length", "mean",
  "versicolor", "Sepal.Length", "max")


calc <- function(spec, var, agg, df) {
  x <- df %>% 
    filter(Species == spec) %>% 
    pull(var)
  do.call(agg, list(x))
} 

pmap_dbl(calculations, calc, df=iris)

```

## Use case: Capture errors

Taken from [https://adv-r.hadley.nz/function-operators.html#safely](https://adv-r.hadley.nz/function-operators.html#safely)


```{r error=TRUE}
x <- list(
  c(0.512, 0.165, 0.717),
  c(0.064, 0.781, 0.427),
  c(0.890, 0.785, 0.495),
  "oops"
)

map_dbl(x, sum)
```

Use function operator:

```{r}
safe_sum <- purrr::safely(sum)

out <- map(x, safe_sum)
str(out)

```


```{r}
out <- purrr::transpose(map(x, safely(sum)))
str(out)
```

```{r}
ok <- map_lgl(out$error, is.null)
ok

out$result[ok]

# Error
x[!ok]

```

This is very useful in the context of fitting models:

```{r eval=FALSE}
fit_model <- function(df) {
  glm(y ~ x1 + x2 * x3, data = df)
}

models <- transpose(map(datasets, safely(fit_model)))
ok <- map_lgl(models$error, is.null)

# which data failed to converge?
datasets[!ok]

# which models were successful?
models[ok]
```




## Use case: Progress indicator

Taken from [https://adv-r.hadley.nz/function-operators.html#fo-case-study](https://adv-r.hadley.nz/function-operators.html#fo-case-study)

```{r}
urls <- c(
  "adv-r" = "https://adv-r.hadley.nz", 
  "r4ds" = "http://r4ds.had.co.nz/"
  # and many many more
)
path <- paste(tempdir(), names(urls), ".html")
if (interactive()) shell.exec(tempdir())
```


```{r}
delay_by <- function(f, amount) {
  force(f)
  force(amount)
  
  function(...) {
    Sys.sleep(amount)
    f(...)
  }
}
system.time(runif(100))
system.time(delay_by(runif, 0.1)(100))

```


```{r}
dot_every <- function(f, n) {
  force(f)
  force(n)
  
  i <- 0
  function(...) {
    i <<- i + 1
    if (i %% n == 0) cat(".")
    f(...)
  }
}
walk(1:100, runif)
walk(1:100, dot_every(runif, 10))
```

```{r}

walk2(
  urls, path, 
  download.file %>% dot_every(1) %>% delay_by(1), 
  quiet = TRUE
)
```


# Metaprogramming

## NSE

Did you ever wonder why you can attach a package without quotation marks?

```{r}
library(purrr)
# instead of
library("purrr")
```

The symbol `purrr` is unknown, so where is it defined?

```{r}
library
```



You'll find this line in the definition of `library`

```{r eval=FALSE}
package <- as.character(substitute(package))
```

This is called non-standard evaluation (NSE).

The evaluation of the parameter `package` does not take place. It is substituted with the name of the passed symbol.

## Quotation and quasiquotation

```{r}
library(rlang)
```

Automatically quote all arguments

```{r}
cement <- function(...) {
  args <- ensyms(...)
  paste(purrr::map(args, as_string), collapse = " ")
}

cement(Good, morning, Daniel)
```

Selectively unquote arguments with `!!`

```{r}
time <- "evening"
name <- "James"

cement(Good, time, name)
cement(Good, !!time, !!name)
```



## Tidy evaluation

How does `dplyr` quotes / unquotes these expressions?

It is very useful for interactive purposes or data analysis scripts, but how to program with it?

```{r}
am_target <- 1
mtcars %>% 
  filter(am == am_target)
```

A quosure holds the expression and the environment where it is used.

```{r}
filter0 <- function(df, var) {
  var <- enquo(var)
  
  df %>% 
    filter(!!var == 0)
}

group_by_at()

mtcars %>% 
  filter0(am)

```


